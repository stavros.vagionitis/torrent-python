import os
import re
import sys
from setuptools import setup, find_packages

# allow only specific python versions
supported_python_versions = ((3, 7), (3, 8),)
py_version = sys.version_info[0:2]

if py_version not in supported_python_versions:
    sys.exit(f'Sorry, Python %s not support: version supported %s' % (
        py_version, supported_python_versions))


regexp = re.compile(r'.*__version__ = [\'\"](.*?)[\'\"]', re.S)

base_package = 'torrent_python'
base_path = os.path.dirname(__file__)

init_file = os.path.join(base_path, 'src', 'torrent_python', '__init__.py')
with open(init_file, 'r') as f:
    module_content = f.read()

    match = regexp.match(module_content)
    if match:
        version = match.group(1)
    else:
        raise RuntimeError(
            'Cannot find __version__ in {}'.format(init_file))

requirements = [
]

test_requirements = [
    'pytest',
    'pytest-asyncio',
]

if __name__ == '__main__':
    setup(
        name='torrent_python',
        description='Torrent Python',
        long_description="",
        license='MIT license',
        version=version,
        install_requires=requirements,
        tests_require=test_requirements,
        keywords=['torrent_python'],
        package_dir={'': 'src'},
        packages=find_packages('src'),
        zip_safe=False,
        include_package_data=True,
        classifiers=['Development Status :: 3 - Alpha',
                     'Intended Audience :: Developers',
                     'Programming Language :: Python :: 3.6'],
        entry_points={
            'console_scripts': ['torrent_python=torrent_python.__main__:run'],
        },
    )
