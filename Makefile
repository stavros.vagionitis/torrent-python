# This makefile has been created to help developers perform common actions.
# Most actions assume it is operating in a virtual environment where the
# python command links to the appropriate virtual environment Python.

MODULE := "torrent_python"

PROJ_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
VENV_DIR := $(PROJ_DIR)/.venv

# Do not remove this block. It is used by the 'help' rule when
# constructing the help output.
# help:
# help: Torrent Python Makefile help
# help:

# help: help                           - display this makefile's help information
.PHONY: help
help:
	@grep "^# help\:" Makefile | grep -v grep | sed 's/\# help\: //' | sed 's/\# help\://'

# help: venv                           - create a virtual environment for development
.PHONY: venv
venv:
	@echo "=== Making venv in $(VENV_DIR)"
	@python3 -m venv "$(VENV_DIR)"
	@/bin/bash -c "source $(VENV_DIR)/bin/activate && pip install pip --upgrade && pip install -r requirements.dev.txt && pip install -e ."
	@echo "Enter virtual environment using:\n\n\t$ source $(VENV_DIR)/bin/activate\n"

# help: clean                          - clean all files using .gitignore rules
.PHONY: clean
clean:
	@git clean -X -f -d

# help: distclean                          - clean all files
.PHONY: distclean
distclean: clean
	@rm -Rf "$(VENV_DIR)"

# help: test                           - run tests
.PHONY: test
test:
	@echo "=== Running unit tests"
	@pytest

# help: test-verbose                   - run tests [verbosely]
.PHONY: test-verbose
test-verbose:
	@pytest -v

# help: check-coverage                 - perform test coverage checks
.PHONY: check-coverage
check-coverage:
	@echo "=== Check coverage"
	@coverage run -m unittest discover -s tests
	@coverage report -m $(MODULE)

# help: style                          - perform code format compliance check
.PHONY: style
style:
	@echo "=== Checking Style"
	@black src/$(MODULE) tests

# help: lint                          - perform code checking
.PHONY: lint
lint:
	@echo "=== Checking Code"
	@echo "\nRunning Pylint against source and test files...\n"
	@pylint --disable=C0103,C0114 *.py src/$(MODULE) tests
	@echo "\nRunning Flake8 against source and test files...\n"
	@flake8 *.py src/$(MODULE) tests
	@echo "\nRunning Bandit against source files...\n"
	@bandit -r *.py src/$(MODULE) tests

# help: check-types                    - check type hint annotations
.PHONY: check-types
check-types:
	@echo "=== Checking Types"
	@cd src; MYPYPATH=$(VENV_DIR)/lib/python*/site-packages mypy -p $(MODULE) --ignore-missing-imports

# help: dist                           - create a wheel distribution package
.PHONY: dist
dist:
	@echo "=== Creating dists"
	@python3 setup.py bdist_wheel

# help: dist-test                      - test a whell distribution package
.PHONY: dist-test
dist-test: dist
	@echo "=== Testing dists"
	@cd dist && ../tests/test-dist.bash ./$(MODULE)-*-py3-none-any.whl

# help: run                            - run the project on localhost
.PHONY: run
run:
	@python3 -m $(MODULE)

# help: checks                            - run all the checks (commit first!!!)
.PHONY: checks
checks: test check-coverage style dist-test


# Keep these lines at the end of the file to retain nice help
# output formatting.
# help:
